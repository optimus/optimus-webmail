function receiveMessage(event)
{
	if (event.data == 'darkmode=true')
		document?.querySelector('.theme.dark').click()
	else if (event.data == 'darkmode=false')
		document?.querySelector('.theme.light').click()
}
window.onmessage = receiveMessage
window.onload = function ()
{
	if (window != window.parent)
	{
		if (document.querySelector('.theme.dark'))
			document.querySelector('.theme.dark').style.display = 'none'
		if (document.querySelector('.theme.light'))
			document.querySelector('.theme.light').style.display = 'none'
		if (document.querySelector('.about'))
			document.querySelector('.about').style.display = 'none'
		if (document.querySelector('.logout'))
			document.querySelector('.logout').style.display = 'none'
		if (document.querySelector('.popover-header'))
			document.querySelector('.popover-header').style.display = 'none'
	}
}