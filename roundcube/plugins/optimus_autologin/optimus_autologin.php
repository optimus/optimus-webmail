<?php
include_once 'JWT.php';
use optimus\JWT\JWT;

class optimus_autologin extends rcube_plugin
{
	public $task = 'login';

	function init()
	{
		$this->load_config();
		$this->add_hook('startup', array($this, 'startup'));
		$this->add_hook('authenticate', array($this, 'authenticate'));
	}

	function startup($args)
	{
		if ($_GET['_task'] == 'logout')
			$args['action'] = 'logout';
		else if (empty($_SESSION['user_id']) && isset($_COOKIE['token']))
			$args['action'] = 'login';

		return $args;
	}

	function authenticate($args)
	{
		if (isset($_COOKIE['token']))
		{
			$payload = (new JWT('{API_SHA_KEY}', 'HS512', 3600, 10))->decode($_COOKIE['token']);
			$args['user'] = $payload['user']->email;
		}

		$rcmail	= rcmail::get_instance();
		$db	= $rcmail->get_dbh();
		$result = $db->query("SELECT password FROM server.users WHERE email = '" . $args['user'] . "'");
		$data = $db->fetch_assoc($result);
		$args['pass'] = $data['password'];

		$args['host'] = "optimus-mail";
		$args['cookiecheck'] = false;
		$args['valid'] = true;
		return $args;
	}

	protected function get_config($key)
	{
	}
}
?>
