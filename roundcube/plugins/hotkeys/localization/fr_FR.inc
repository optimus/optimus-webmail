<?php

$labels = array();

$labels['plugin_hotkeys'] = 'Plugin: Raccourcis clavier';

$labels['hotkeys'] = 'Hot Keys';
$labels['activate_plugin'] = 'Activer le plugin';
$labels['enable_logging'] = 'Activer les logs';
$labels['enable_button'] = 'Activer le bouton dans le menu';
$labels['enable_prevent'] = 'Désactiver les touches par défaut du navigateur';
$labels['plugin_hotkey'] = 'Touche pour ouvrir le menu';
$labels['reset_to_default'] = 'Réinitialiser la configuration par défaut';

$labels['button_text'] = 'Hotkeys';
$labels['button_title'] = 'Configurer les raccourcis clavier';

$labels['command_hotkeys'] = 'Commande Raccourcis Clavier';
$labels['command_mapping'] = 'Commande Affectation';

$labels['mapping'] = 'Affectation';

$labels['profile'] = 'Profile';
$labels['command'] = 'Commande';
$labels['context'] = 'Contexte';
$labels['key'] = 'Touche';
$labels['comment'] = 'Commentaire';
$labels['script'] = 'Script';
$labels['source'] = 'Source';

$labels['tooltip_profile'] = 'Profile à qui la commande appartient';
$labels['tooltip_command'] = 'Commande à lancer';
$labels['tooltip_context'] = 'Contexte dans lequel la commande peut être lancée';
$labels['tooltip_key_enter'] = 'Raccourci clavier à utiliser, par exemple : ctrl+f3';
$labels['tooltip_key_click'] = 'Cliquez ici et ensuite appuyez sur la combinaison de touches souhaitée';
$labels['tooltip_script'] = 'Code javascript optionnel qui surpasse la commande par défaut';
$labels['tooltip_comment'] = 'Description de la commande exécutée avec ce raccourci clavier';

$labels['all'] = 'Tous';
$labels['active'] = 'Actifs';
$labels['passive'] = 'Passifs';
$labels['custom'] = 'Personnalisés';
$labels['internal'] = 'Internes';
$labels['external'] = 'Externes';
$labels['undefined'] = 'Non définis';

$labels['apply'] = 'Appliquer';
$labels['update'] = 'Mise à jour';
$labels['create'] = 'Créer';
$labels['remove'] = 'Supprimer';
$labels['invoke']  = 'Invoquer';
$labels['close'] = 'Fermer';

$labels['new'] = 'Nouveau';
$labels['change'] = 'Modifier';
$labels['export'] = 'Export';
$labels['import'] = 'Import';
$labels['share'] = 'Partage';

$labels['combine'] = 'Combiner';

$labels['import_mapping'] = 'Importer des profils';
$labels['export_mapping'] = 'Exporter ses profils';
$labels['share_mapping'] = 'Partager ses profils';
$labels['share_via_imap'] = 'Boite mail publique';
$labels['share_via_mail'] = 'En pièce jointe d\'un mail';

$labels['success'] = 'Succès';
$labels['failure'] = 'Echec';

$labels['profile_list'] = 'Liste des profils';

$labels['feature_choice'] = 'Activer/désactiver des fonctionnalités';

$labels['custom_command_list'] = 'Liste des commandes personnalisées';
$labels['internal_command_list'] = 'Liste des commandes internes';
$labels['supported_base_keys'] = 'Touches de base supportées';
$labels['supported_meta_keys'] = 'Touches de combinaison supportées';
$labels['prevent_default_keys'] = 'Touches par défaut du navigateur à désactiver';

?>
