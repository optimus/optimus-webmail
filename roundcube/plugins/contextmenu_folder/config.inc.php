<?php
$config['plugin.contextmenu_folder.enable_logging'] = false;
$config['plugin.contextmenu_folder.feature_choice'] = array(
        // 'remember_filter',
        // 'remember_mailbox',
        // 'remember_message',
        'track_on_create',
        'track_on_delete',
        'track_on_rename', 
        'track_on_locate',
        'filter_on_mbox_mark_read',
        'render_selected',
        'render_transient',
        'replace_menu_purge',
        // 'allow_purge_any',
        // 'allow_purge_junk',
        'allow_purge_trash',
        // 'allow_purge_regex',
        // 'hide_menu_link',
        // 'hide_ctrl_menu',
        // 'hide_mbox_menu',
        // 'hide_mesg_menu',
        'expire_transient',
        'filter_on_expire_transient',
        'footer_contextmenu',
);
?>