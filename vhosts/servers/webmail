server
{
	listen 80;
	listen [::]:80;
	server_name webmail.{DOMAIN};
	return 301 https://$host$request_uri;
}

server
{
	listen 443 ssl http2;
	listen [::]:443 ssl http2;
	server_name webmail.{DOMAIN};

	ssl_certificate /etc/letsencrypt/live/{DOMAIN}/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/{DOMAIN}/privkey.pem;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
	ssl_ciphers HIGH:!aNULL:!MD5;

	access_log /var/log/optimus/access-optimus-webmail.log;
	error_log /var/log/optimus/error-optimus-webmail.log;

	error_page 502 /index.php;

	index /index.php;
	
	location ~ ^/(README.md|INSTALL|LICENSE|CHANGELOG|UPGRADING)$ 
	{
		deny all;
	}

	location ~ ^/(config|temp|logs)/ 
	{
		deny all;
	}

	location ~ /\. 
	{
		deny all;
		access_log off;
		log_not_found off;
	}

	location ~ \.php$ 
	{
		fastcgi_pass {CONTAINER_IP}:9000;
		fastcgi_index index.php;
		fastcgi_param SCRIPT_FILENAME /srv/roundcube/index.php;
		include fastcgi_params;
		access_log /var/log/optimus/access-{NAME}.log;
		error_log /var/log/optimus/error-{NAME}.log;
		client_max_body_size 20M;
	}

	location /
	{
		try_files $uri $uri/ @phpproxy;
		client_max_body_size 20M;
	}

	location @phpproxy 
	{
		proxy_pass http://{CONTAINER_IP};
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Host $server_name;
		recursive_error_pages on;
		error_page 404 = @rewrite_proxy;
	}

	location @rewrite_proxy 
	{
		rewrite ^/(.*)$ /index.php?$1 break;
		proxy_pass http://{CONTAINER_IP};
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Host $server_name;
	}
}