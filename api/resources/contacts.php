<?php
$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'positive_integer', true);
	$input->owner_email = get_user_email($input->owner);

	$input->body->firstname = check('firstname', $input->body->firstname, 'string', false);
	$input->body->lastname = check('lastname', $input->body->lastname, 'string', false);
	$input->body->company_name = check('company_name', $input->body->company_name, 'string', false);
	$input->body->email = check('email', $input->body->email, 'email', false);

	if (!isset($input->body->email) AND !isset($input->body->pro_email))
		return array("code" => 400, "message" => 'Vous devez renseigner un email ou pro_email');
	
	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un contact chez cet utilisateur");

	//FIND ROUNDCUBE USER ID ATTACHED TO THE CURRENT OPTIMUS OWNER ID
	$roundcube_user_id_query = $connection->prepare("SELECT user_id FROM roundcube.users WHERE username = :username AND mail_host = 'optimus-mail'");
	$roundcube_user_id_query->bindParam(':username', $input->owner_email, PDO::PARAM_STR);
	if($roundcube_user_id_query->execute())
	{
		$roundcube_user_id = $roundcube_user_id_query->fetch(PDO::FETCH_ASSOC);
		if (!$roundcube_user_id['user_id'])
		return array("code" => 400, "message" => 'Aucun utilisateur roundcube ne correspond à cet utilisateur optimus');
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);

	$input->body->changed = date('Y-m-d H:i:s');

	if ($input->body->company_name != "")
		$input->body->name = $input->body->company_name;
	else if ($input->body->firstname != "")
		$input->body->name = $input->body->firstname . ($input->body->lastname ? ' ' . $input->body->lastname : '');
	else if ($input->body->lastname != "")
		$input->body->name = $input->body->lastname;
	else if ($input->body->email != "")
		$input->body->name = $input->body->email;
	else if ($input->body->pro_email != "")
		$input->body->name = $input->body->pro_email;

	if (isset($input->body->pro_email))
		$input->body->name .= ' (pro)';

	if (isset($input->body->pro_email))
		$input->body->email = $input->body->pro_email;

	if (!isset($input->body->firstname))
		$input->body->firstname = '';

	if ($input->body->company_name != "")
	{
		$input->body->surname = $input->body->company_name;
		$input->body->firstname = '';
	}
	else if (isset($input->body->lastname))
		$input->body->surname = $input->body->lastname;
	else
		$input->body->surname = '';

	$input->body->words = $input->id . ' ' . str_replace('(pro)', 'pro', $input->body->name);
	
	//IF ENTRY EXISTS UPDATE, OTHERWISE INSERT
	$already_exists = $connection->query("SELECT contact_id FROM roundcube.contacts WHERE user_id = '" . $roundcube_user_id['user_id'] . "' AND words LIKE '" . $input->id . " %' AND words " . (isset($input->body->pro_email) ? 'LIKE' : 'NOT LIKE') . " '% pro'")->fetch(PDO::FETCH_ASSOC);
	
	if (isset($already_exists['contact_id']))
		$query = $connection->prepare("UPDATE roundcube.contacts SET changed = :changed, name = :name, firstname = :firstname, surname = :surname, email = :email, words = :words, user_id = :user_id WHERE contact_id = :contact_id");
	else
		$query = $connection->prepare("INSERT INTO roundcube.contacts SET changed = :changed, name = :name, firstname = :firstname, surname = :surname, email = :email, words = :words, user_id = :user_id");
	
	$query->bindParam(':changed', $input->body->changed, PDO::PARAM_STR);
	$query->bindParam(':name', $input->body->name, PDO::PARAM_STR);
	$query->bindParam(':firstname', $input->body->firstname, PDO::PARAM_STR);
	$query->bindParam(':surname', $input->body->surname, PDO::PARAM_STR);
	$query->bindParam(':email', $input->body->email, PDO::PARAM_STR);
	$query->bindParam(':words', $input->body->words, PDO::PARAM_STR);
	$query->bindParam(':user_id', $roundcube_user_id['user_id'], PDO::PARAM_INT);
	if (isset($already_exists['contact_id']))
		$query->bindParam(':contact_id', $already_exists['contact_id'], PDO::PARAM_INT);

	if($query->execute())
		return array("code" => 201);
	else
	{
		error_log($query->errorInfo()[2]);
		return array("code" => 400, "message" => $query->errorInfo()[2]);
	}
};

$delete = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->owner_email = get_user_email($input->owner);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un contact chez cet utilisateur");

	//FIND ROUNDCUBE USER ID ATTACHED TO THE CURRENT OPTIMUS OWNER ID
	$roundcube_user_id_query = $connection->prepare("SELECT user_id FROM roundcube.users WHERE username = :username AND mail_host = 'optimus-mail'");
	$roundcube_user_id_query->bindParam(':username', $input->owner_email, PDO::PARAM_STR);
	if($roundcube_user_id_query->execute())
	{
		$roundcube_user_id = $roundcube_user_id_query->fetch(PDO::FETCH_ASSOC);
		if (!$roundcube_user_id['user_id'])
		return array("code" => 400, "message" => 'Aucun utilisateur roundcube ne correspond à cet utilisateur optimus');
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);

	if ($connection->query("DELETE FROM roundcube.contacts WHERE user_id = '" . $roundcube_user_id['user_id'] . "' AND words LIKE '" . $input->id . " %' "))
		return array("code" => 200);
	else
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
};
?>