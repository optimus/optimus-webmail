<?php
$get = function ()
{
	global $connection;
	auth();
	allowed_origins_only();
	$service_name = 'optimus-webmail';

	$query = $connection->prepare("SELECT status, manifest FROM `server`.`services` WHERE name = :name");
	$query->bindParam(":name", $service_name);
	$query->execute();
	$service = $query->fetch(PDO::FETCH_OBJ);
	$output = json_decode($service->manifest, false);
	$output->status = $service->status;

	return array("code" => 200, "data" => $output);
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-webmail';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut activer ce service");
	
	$is_installed = $connection->prepare("SELECT * FROM server.users_services WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'");
	$is_installed->execute();
	if ($is_installed->rowCount() != 0)
		return array("code" => 400, "message" => "Le service " . $service_name . " est déjà activé pour cet utilisateur");

	if (!$connection->query("REPLACE INTO `server`.`users_services` SET user = '" . $input->owner . "', service = '" . $service_name . "'"))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);	
		
	return array("code" => 201);
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-webmail';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut désactiver un service");

	if ($_GET['remove_data'] == true)
	{
		//il faudrait ici supprimer les lignes concernant l'utilisateur dans les bases 'roundcube' et 'mailserver'
	}
		
	if (!$connection->query("DELETE FROM `server`.`users_services` WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'"))
		$errors[] = $connection->errorInfo()[2];
	
	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 200);
};
?>