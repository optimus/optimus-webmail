

export default class OptimusWebmail
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-webmail/administration/webmail.html', this.target)
		loader(this.target)

		let smtp = await rest('https://' + store.user.server + '/optimus-base/server/preferences/smtp', 'GET', {}, null).then(result => result.code == 200 && result.data) || '404'

		this.target.querySelectorAll('input[name="webmail_smtp"]').forEach(radio => 
		{
			if (radio.value == smtp)
				radio.checked = true
		})

		rest(store.user.server + '/optimus-base/ovh/' + store.user.server.replace('api.', '') + '/email', 'GET')
			.then(response =>
			{
				let email = response.data
				if (email?.offer)
				{
					this.target.querySelector('.ovh_unavailable_message').classList.add('is-hidden')
					this.target.querySelector('.ovh_radio').removeAttribute('disabled')
				}
				loader(this.target, false)
			})

		this.target.querySelector('.local_radio').onclick = () => this.change_smtp('local')
		this.target.querySelector('.ovh_radio').onclick = () => this.change_smtp('ovh')
	}

	change_smtp(value)
	{
		rest('https://' + store.user.server + '/optimus-base/server/preferences/smtp', 'POST', value)
			.then(response => 
			{
				if (response.code == 201)
				{
					optimusToast('Redémarrage du webmail en cours...', 'is-warning')
					rest('https://' + store.user.server + '/optimus-base/services/optimus-webmail/start', 'POST')
						.then(response => response.code == 200 && optimusToast('Le webmail a redémarré avec succès', 'is-success'))
				}
			})
	}
}