

export default class OptimusWebmail
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-webmail/webmail/index.html', this.target)

		main.classList.add('p-0')
		this.leftmenuStatus = store.leftmenu.status

		store.leftmenu.previous_status = store.leftmenu.status
		if (store.leftmenu.status == 'visible' && window.matchMedia('screen and (max-width : 1023px)'))
			leftmenu.toggle('hidden')

		return fetch("https://" + store.user.server.replace('api', 'webmail') + '/plugins/optimus-base_autologin/set_darkmode.php?darkmode=' + store.darkmode,
			{
				mode: 'no-cors',
				credentials: 'include'
			})
			.then(() => 
			{
				document.getElementById('webmail_container').src = "https://" + store.user.server.replace('api', 'webmail') + '/?_task=mail&_mbox=INBOX'
				document.getElementById('webmail_container').onload = () => document.getElementById('webmail_container').contentWindow.postMessage('darkmode=' + store.darkmode, "https://" + store.user.server.replace('api', 'webmail'))
			})
	}

	onUnload()
	{
		document.getElementById('webmail_container').contentWindow.postMessage('logout', "https://" + store.user.server.replace('api', 'webmail'))
		main.classList.remove('p-0')
		if (store.innerWidth >= 1280)
			leftmenu.toggle(this.leftmenuStatus)
	}
}