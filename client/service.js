export default class OptimusWebmailService
{
	constructor()
	{
		this.name = 'optimus-webmail'
		store.services.push(this)

		if (!admin_only())
			this.optimus_base_administration_tabs =
				[
					{
						id: "tab_webmail",
						text: "Webmail",
						link: "/services/optimus-webmail/administration/webmail.js",
						position: 660
					},
				]
	}

	login()
	{
		leftmenu.create('optimus-cloud', 'CLOUD')
		leftmenu['optimus-cloud'].add('email', 'Emails', 'fas fa-envelope', () => router('optimus-webmail/webmail'))

		this.websocketIncomingListener = message => 
		{
			if (message.detail.command == 'mail_folders_updated')
			{
				let refresher = document.createElement('iframe')
				refresher.src = 'https://' + store.user.server.replace('api', 'webmail') + '/?_task=settings&_action=folders'
				refresher.style.display = 'none'
				document.body.appendChild(refresher)
				setTimeout(() => refresher.remove(), 1000)
			}
		}
		optimusWebsocket.addEventListener('incoming', this.websocketIncomingListener)
	}

	logout()
	{
		leftmenu['optimus-cloud'].querySelector('#email').remove()
		store.services = store.services.filter(service => service.name != this.id)

		let logouter = document.createElement('iframe')
		logouter.src = 'https://' + store.user.server.replace('api', 'webmail') + '/?_task=logout'
		logouter.style.display = 'none'
		main.appendChild(logouter)

		optimusWebsocket.removeEventListener('incoming', this.websocketIncomingListener)
	}

	dashboard()
	{
		quickactions_add('quickaction-cloud-header', 'header', 'CLOUD')
		quickactions_add('quickaction-cloud-email-create', 'item', 'Ecrire un courriel', 'fas fa-envelope', () => router('optimus-webmail/webmail/compose'))
	}
}