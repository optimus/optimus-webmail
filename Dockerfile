FROM php:8.1-fpm-alpine

RUN apk --no-cache add shadow git tzdata unzip gettext \
	&& docker-php-ext-install pcntl \
	&& usermod --uid 219 www-data && groupmod --gid 219 www-data \
	&& groupadd mailboxes --gid 203 && useradd --uid 203 mailboxes -g mailboxes -s /bin/false -d /srv/mailboxes 

RUN apk --no-cache add bash coreutils rsync aspell aspell-en gnupg curl apache2 \
	&& apk --no-cache add --virtual .build-deps $PHPIZE_DEPS icu-dev freetype-dev imagemagick-dev libjpeg-turbo-dev libpng-dev libzip-dev libtool openldap-dev aspell-dev \
	&& docker-php-ext-configure gd --with-jpeg --with-freetype \
	&& docker-php-ext-configure ldap \
	&& docker-php-ext-install exif gd intl ldap pdo_mysql zip pspell pcntl \
	&& pecl install imagick redis \
	&& docker-php-ext-enable imagick opcache redis \
	&& runDeps="$( scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions | tr ',' '\n' | sort -u | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' )" \
	&& apk add --virtual .roundcubemail-phpext-rundeps imagemagick $runDeps \
	&& apk del .build-deps \
	&& rm -rf /tmp/pear \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p /srv/roundcube \
	&& curl -o /tmp/roundcubemail.tar.gz -fSL https://github.com/roundcube/roundcubemail/releases/download/1.6.10/roundcubemail-1.6.10-complete.tar.gz \
	&& tar -xf /tmp/roundcubemail.tar.gz -C /srv/roundcube --strip-components=1 --no-same-owner \
	&& rm -r /tmp/roundcubemail.tar.gz \
	&& rm -r /srv/roundcube/installer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1
COPY ./optimus-webmail/composer.json /srv/roundcube/
RUN /usr/bin/composer --no-interaction --no-dev --working-dir=/srv/roundcube --optimize-autoloader update

#DISABLE CSRF FOR LOGOUT
RUN sed -i '/$RCMAIL->request_security_check(rcube_utils::INPUT_GET | rcube_utils::INPUT_POST);/d' /srv/roundcube/index.php


#THIS DEACTIVATES SQL DB UPDATE FOR ROUNDCUBE PLUGINS BECAUSE IT DOESN'T WORK IN DOCKER BUILD ENVIRONMENT
#RUN sed -i '/rcmail_utils::db_init($sqldir);/d' /srv/roundcube/vendor/roundcube/plugin-installer/src/ExtensionInstaller.php
#REQUIRE ADDITIONNAL PLUGINS THAT USUALLY NEED SQL DB UPDATE
#RUN /usr/bin/composer --working-dir=/srv/roundcube require toteph42/identy_switch

RUN sed -i "s#mysql://username:password@localhost/database#mysql://{MARIADB_API_USER}:{MARIADB_API_PASSWORD}@optimus-databases/mailserver#g" /srv/roundcube/plugins/sauserprefs/config.inc.php \
	&& sed -i 's/<head>/<head>\n<script src="\/templates\/includes\/optimus.js" defer><\/script>/g' /srv/roundcube/skins/elastic/templates/includes/layout.html \
	&& sed -i 's/#21292c/#282c34/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#21292c/#282c34/g' /srv/roundcube/skins/elastic/watermark.html \
	&& sed -i 's/#00acff/#209cee/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#37beff/#209cee/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#007bb7/#4f545e/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#4d6066/#353942/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#374549/#4c7de1/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/;color:#209cee/;color:#c5d1d3/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/{color:#209cee/{color:#c5d1d3/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#8ba3a7/#d7dae0/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#006a9d/#4c7de1/g' /srv/roundcube/skins/elastic/styles/styles.min.css \
	&& sed -i 's/#1eb6ff/#4c7de1/g' /srv/roundcube/skins/elastic/styles/styles.min.css


COPY ./optimus-webmail/api /srv/api
COPY ./optimus-webmail/client /srv/client
COPY ./optimus-webmail/roundcube /srv/roundcube
COPY ./optimus-webmail/sql /srv/sql
COPY ./optimus-webmail/vhosts /srv/vhosts

COPY ./optimus-webmail/manifest.json /srv/manifest.json

COPY ./optimus-webmail/php.ini /usr/local/etc/php/conf.d
COPY ./optimus-libs/init.php /srv/init.php
COPY ./optimus-webmail/httpd.conf /etc/apache2/httpd.conf

RUN mkdir -p /srv/api/libs
COPY \
	./optimus-libs/datatables.php \
	./optimus-libs/functions.php \
	./optimus-libs/JWT.php \
	./optimus-libs/ovh.php \
	./optimus-libs/websocket_client.php \
	/srv/api/libs/

RUN chown www-data:www-data -R /var/www \
	&& chown www-data:www-data -R /var/log/apache2 \
	&& chown www-data:www-data -R /run/apache2 \
	&& chown www-data:www-data -R /srv/roundcube

ENTRYPOINT ["php", "/srv/init.php"]